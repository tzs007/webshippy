import React from "react";
import ReactDOM from "react-dom";
import Blog from "./Blog";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  <React.StrictMode>
    <Blog />
  </React.StrictMode>,
  document.getElementById("root")
);
