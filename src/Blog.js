import Posts from "./components/Posts";

function App() {
  return (
    <div className="container">
      <div className="row mt-4">
        <div className="col">
          <h1 className="mb-3">Blog</h1>
          <hr />
          <Posts />
        </div>
      </div>
    </div>
  );
}

export default App;
