import React, { useState, useEffect, useCallback } from "react";

const Posts = () => {
  const [posts, setPosts] = useState([]);

  const fetchPosts = async () => {
    try {
      const response = await fetch("http://localhost:3000/posts.json", {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      });

      if (response.status === 200) {
        return response.json();
      } else {
        alert("Server error:(");
      }
    } catch (error) {
      alert("Fetch error:(");
    }
  };

  useEffect(() => {
    (async () => {
      let posts = await fetchPosts();

      if (posts.length > 0) {
        posts.map((post) => (post.author = "Steve Jobs"));
        posts.sort((a, b) => new Date(b.date) - new Date(a.date));
        setPosts(posts);
      }
    })();
  }, []);

  return (
    <div>
      {posts ? (
        posts.map((post, index) => (
          <div className="card mb-3" key={`post-card-${index}`}>
            <div className="card-header h2">{post.title}</div>
            <div className="card-body">{post.body}</div>
            <div className="card-footer d-flex justify-content-between">
              <time dateTime={post.date}>Published at {post.date}</time>
              <div rel="author">Written by {post.author}</div>
            </div>
          </div>
        ))
      ) : (
        <div className="alert">No posts fetched.</div>
      )}
    </div>
  );
};

export default Posts;
